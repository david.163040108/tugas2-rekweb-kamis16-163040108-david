-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2018 at 05:19 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tuku`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` tinyint(1) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(10) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `deskripsi` varchar(1000) NOT NULL,
  `harga` int(100) NOT NULL,
  `stok` int(200) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `kategori` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `nama_barang`, `deskripsi`, `harga`, `stok`, `gambar`, `kategori`) VALUES
(37, 'OPPO A83 2GB Gold', '<p>\r\n\r\nDeskripsi</p><ul><li>Prosesor: Mediatek MT6763T Helio P23 Octa-core 2.5 GHz</li><li>Kapasitas: 16GB</li><li>RAM: 2GB</li><li>Ukuran Layar: 5.7 inch</li><li>Kamera Belakang: 13MP</li><li>Kamera Depan: 8MP</li><li>Baterai: 3180 mAh</li><li>Sistem Operasi: Android 7.1 Nougat</li></ul>\r\n\r\n<br><p></p>', 1999000, 50, 'gambar1.jpg', ''),
(38, 'TOSHIBA 55 Inch Pro Theatre Series Android TV UHD', '<p>\r\n\r\nDeskripsi</p><ul><li>Ukuran: 55 Inch</li><li>Resolusi: 3840 x 2160 (Ultra HD 4K)</li><li>Picture Engine: CEVO 4K Engine</li><li>Audio: Dolby Digital Plus</li><li>AMR: AMR+800</li><li>Android TV</li><li>Android OS</li><li>Core System: 8</li><li>RAM: 2.5GB</li><li>Internal Memory: 16GB</li><li>Tuner: DVB-T2/T/C &amp; Analog</li><li>Konektivitas: HDMI/USB/Wireless LAN</li></ul>Kelengkapan<ul><li>Unit Utama</li></ul>Garansi1 Year (Local Official Distributor Warranty)\r\n\r\n<br><p></p>', 12449000, 10, '5a6ff6f66fa8f.jpg', ''),
(39, 'SHARP Kulkas 1 Pintu', '<p>\r\n\r\nDeskripsi</p><ul><li>Kulkas 1 Pintu</li><li>Tempered Glass Tray</li><li>Direct Cooling System</li><li>Washable Gasket</li><li>Semi Automatic Defrost System</li><li>Capacity (Gross/Netto): 133 Liter/128 Liter</li><li>84 Watt</li></ul>\r\n\r\n<br><p></p>', 1549000, 5, 'SHARP-Kulkas-1-Pintu-SJ-N166F-FB-Flower-Blue-3318179396-2017721145951.jpg', ''),
(40, 'COSMOS Stand Fan ', '<p>\r\n\r\nDeskripsi</p><ul><li>Kipas Angin</li><li>Hembusan Angin Kencang</li><li>Baling\" Kokoh</li><li>Dilengkapi 3 Pilihan Kecepatan Kipas Angin</li><li>Kinerja Motor Lebih Lancar</li><li>Design Elegan</li><li>Watt Rendah</li><li>Daya Masukan 50 watt</li><li>16\"</li></ul>\r\n\r\n<br><p></p>', 275000, 50, 'kipas.jpg', ''),
(41, 'ELECTROLUX AC Split Polar', '<p>\r\n\r\nDeskripsi</p><ul><li>AC Split</li><li>Self Diagnoses</li><li>I Feel</li><li>Turbo Fan Speed</li></ul>\r\n\r\n<br><p></p>', 7925000, 88, 'ac.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(60) NOT NULL,
  `group` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `group`) VALUES
(1, 'admin', 'admin', 1),
(2, 'tayo', '123', 1),
(3, 'david', '123', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
