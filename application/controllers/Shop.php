<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

	public function index()
	{
		$this->load->model('Crud_model');
		$data['produk'] = $this->Crud_model->getProduk();
		$data['content'] = 'home';
		$this->load->view('templates_home/template', $data);

    }


    public function detail($id){
        $this->load->model('Crud_model');
        $data['detail'] = $this->Crud_model->getElement($id);
        $data['content'] = 'detail';
        $this->load->view('templates_home/template', $data);
    }

    public function add_to_cart($id)
    {
    	$this->load->model('Crud_model');
    	$produk = $this->Crud_model->getElement($id);
    	$data = array(
        'id'      => $produk->id,
        'qty'     => 1,
        'price'   => $produk->harga,
        'name'    => $produk->nama_barang
		);

		$this->cart->insert($data);
		redirect('shop');
    }

    public function cart(){
    	
    	$data['content'] = 'show_cart';
    	$this->load->view("templates_home/template",$data);
    }

    public function clear_cart()
    {
    	$this->cart->destroy();
    	redirect('shop/cart');
    }

    public function hapusCart($rowid){
		$this->cart->update(array('rowid' => $rowid, 'qty'=>0));
		$i = 0;
		foreach ($this->cart->contents() as $key) {
			$i++;
		}
		$data["total_cart"] = $i;
		$data['content'] = 'show_cart';
		$this->load->view('templates_home/template', $data);
	}

}