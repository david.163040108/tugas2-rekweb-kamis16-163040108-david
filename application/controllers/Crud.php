<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Controller {

	public function index()
	{
		$this->load->model('Crud_model');
		$data['produk'] = $this->Crud_model->getProduk();
		$data['content'] = 'crud';
		$this->load->view('templates/template', $data);

	}


	public function ubah2(){
		$data['content'] = 'ubah';
		$this->load->view('templates/template', $data);
	}

	public function add(){
		$data['content'] = 'tambah';
		$this->load->view('templates/template', $data);
	}

	public function tambah() {
		$this->_rules();

		$nama_barang = $this->input->post('nama_barang');
		$harga = $this->input->post('harga');
		$stok = $this->input->post('stok');
		//$gambar = $this->input->post('gambar');
		$deskripsi = $this->input->post('deskripsi');
		$this->load->model('Crud_model');

		if($this->form_validation->run() == FALSE) {
		$this->add();

		}else{

				$config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = '300';
                $config['max_width']            = '3000';
                $config['max_height']           = '3000';

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload())
                {
                	$this->add();
                }else{

                $file = $this->upload->data();
            	$gambar = $file['file_name'];
					$data = array(
						'nama_barang' => $nama_barang,
						'harga' => $harga,
						'stok' => $stok,
						'gambar' => $gambar,
						'deskripsi' => $deskripsi
					);	

					$status = $this->Crud_model->insertProduk($data);

					if ($status) {
						$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                <h4><i class="icon fa fa-check"></i> Alert!</h4>
		                Produk Berhasil diTambah
		              </div>');
					}else{
						$this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
		                Produk gagal di Tambahkan
		              </div>');
					}
				redirect('crud');
                }	
			
		}
	}


	public function hapus($id) {

	$this->load->model('Crud_model');
	$status = $this->Crud_model->hapusProduk($id);
			if ($status) {
				$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                Produk Berhasil diHapus
              </div>');
			}else{
				$this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                Produk gagal di Hapus
              </div>');
			}
			redirect('crud');
	}	

	public function _rules()
	{
		$this->form_validation->set_rules('nama_barang', 'nama produk', 'trim|required');
		$this->form_validation->set_rules('harga', 'harga', 'trim|required');
		$this->form_validation->set_rules('stok', 'stok', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}



	public function ubah($id) {
		$this->_rules();


		$nama_barang = $this->input->post('nama_barang');
		$harga = $this->input->post('harga');
		$stok = $this->input->post('stok');
		$deskripsi = $this->input->post('deskripsi');
		$this->load->model('Crud_model');
		$data['prd'] = $this->Crud_model->getid($id);

		if($this->form_validation->run() == FALSE) {
		$data['prd'] = $this->Crud_model->getid($id);
		$data['content'] = 'ubah';
		$this->load->view('templates/template', $data);

		}else{
			if($_FILES['userfile']['name'] != '')
			{
				$config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = '300';
                $config['max_width']            = '3000';
                $config['max_height']           = '3000';

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload())
                {
                	$this->ubah2();
                }else{

                $file = $this->upload->data();
            	$gambar = $file['file_name'];
					$prd = array(
						'nama_barang' => $nama_barang,
						'harga' => $harga,
						'stok' => $stok,
						'gambar' => $gambar,
						'deskripsi' => $deskripsi,
						'id' => $id
					);	

					$status = $this->Crud_model->ubah($prd);

					if ($status) {
						$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                <h4><i class="icon fa fa-check"></i> Alert!</h4>
		                Produk Berhasil diTambah
		              </div>');
					}else{
						$this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
		                Produk gagal di Tambahkan
		              </div>');
					}
				redirect('crud');
                }	
			}else{

				$prd = array(
						'nama_barang' => $nama_barang,
						'harga' => $harga,
						'stok' => $stok,
						'deskripsi' => $deskripsi,
						'id' => $id
					);	

					$status = $this->Crud_model->ubah($prd);

					if ($status) {
						$this->session->set_flashdata('status', '<div class="alert alert-success alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                <h4><i class="icon fa fa-check"></i> Alert!</h4>
		                Produk Berhasil diTambah
		              </div>');
					}else{
						$this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
		                Produk gagal di Tambahkan
		              </div>');
					}
					redirect('crud');
			}
		}
		// $data['content'] = 'ubah';
		// $this->load->view('templates/template', $data);
	}
//========================================================================================================================
}  
