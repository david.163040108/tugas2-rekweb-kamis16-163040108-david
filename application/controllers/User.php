<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function index()
	{	
		$this->form_validation->set_rules('username','Username', 'required|alpha_numeric');		
		$this->form_validation->set_rules('password','Password', 'required|alpha_numeric');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('login');
		}else{
			$this->load->model('User_model');
			$valid_user = $this->User_model->check_credential();

			if ($valid_user == FALSE) {
				$this->session->set_flashdata('error','wrong Username / Password');
				redirect('user');
			}else{
				$this->session->userdata('username', $valid_user->username);
				$this->session->userdata('group', $valid_user->group); 
				switch ($valid_user->group) {
					case '1':
						redirect(base_url());
						break;
					case '2':
						redirect('shop');
						break;
					
					default:
						break;
				}
			}
		}
	}



}