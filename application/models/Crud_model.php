<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_model extends CI_Model {

	public function getProduk(){
		return $this->db->get('produk')->result();
	}

	public function insertProduk($data){
		$this->db->insert('produk', $data);
		return $this->db->affected_rows();
	}


	public function hapusProduk($id) {
		$this->db->where('id', $id);
		$this->db->delete('produk');
		return $this->db->affected_rows();
	}


    public function ubah($prd)
    {
        $this->db->where('id', $prd['id']);
        return $this->db->update('produk', $prd);
    }


    public function getid($id)
    {
        $query = $this->db->get_where('produk', ['id' => $id]);
        return $query->row_array();
    }

   	public function getElement($id){
		return $this->db->get_where('produk', array('id' => $id))->row();
	}


}