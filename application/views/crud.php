

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Produk Tabel
        <small>Elektronik</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    <?= $this->session->flashdata('status'); ?>
    
     <div class="box">
       <!-- button tambah -->
            <div class="box-header">
              <a href="<?= base_url('crud/add'); ?>"><button type="button" class="btn btn-sm btn-success "><span class="fa fa-database"></span> Tambah Data</button></a>
            </div>

            <div class="box-header">
              <a href="<?= base_url(); ?>"><button type="button" class="btn btn-sm btn-success ">Shopping</button></a>
            </div>
            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped display">
                <thead>
                <tr>
                  <th>Gambar</th>
                  <th>Nama Barang</th>
                  <th>Deskripsi</th>
                  <th>Harga</th>
                  <th>Stok</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($produk as $td) : ?>
                <tr>
                  <td>
                    <img src="<?php echo base_url('')?>/uploads/<?=$td->gambar ?>" style="width:40px;">
                  </td>
                  <td><?= $td->nama_barang?></td>
                  <td><?=$td->deskripsi?></td> 
                  <td><?='Rp. '.number_format($td->harga)?></td>
                  <td><?=$td->stok?></td>
                  <td class="text-center">
                    <a href="<?php echo site_url('crud/hapus/'.$td->id.'') ?>" class="btn btn-sm btn-danger fa fa-trash-o"></a>

                    <a href="<?= site_url('crud/ubah/'.$td->id.'')?>" class="btn btn-sm btn-success fa fa-edit"></a>
                  </td>
                </tr>
                <?php endforeach  ?>
                <tfoot>
                <tr>
                  <th>Gambar</th>
                  <th>Nama Barang</th>
                  <th>Deskripsi</th>
                  <th>Harga</th>
                  <th>Stok</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- ========================================================================================================================== -->

 