<div class="container mt-5">
<!-- <div class="container mt-5"> -->
	<div class="row">
		<div class="col">
				<img src="<?= base_url() ?>/uploads/<?= $detail->gambar ?>" class="rounded float-left" alt="..." style="width :100%">
		</div>
		<div class="col-6 animated fadeInUp slow">
			<h2><?= $detail->nama_barang?></h2>
			<h5 class="text-secondary"><?= $detail->stok?></h5>
			<br>
				<p class="text-danger">Informasi Prodak</p>
					<p><?= $detail->deskripsi ?></p>
		</div>

		<div class="col">
			<div class="card border-info mb-3" style="max-width: 18rem;">
			  <div class="card-header font-weight-bold text-primary"><h6>Total Harga</h6></div>
			  <div class="card-body text-info">
			    <h5 class="card-title text-danger" ><?='Rp. '.number_format($detail->harga)?></h5>
			  </div>
			</div>
			<a href="<?= base_url('shop/add_to_cart/'. $detail->id) ?>">
			<button type="button" class="btn btn-primary btn-lg btn-block mb-2">Beli Sekarang</button></a>
			<a href="<?= base_url() ?>" class="btn btn-danger btn-lg btn-block" role="button" aria-pressed="true">Kembali</a>
		</div>
	</div>
<!-- </div> -->
</div>