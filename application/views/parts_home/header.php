<!DOCTYPE html>
<html>
<head>
  <title>Shope</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/Ionicons/css/ionicons.min.css') ?>">
</head>
<body>

<nav class="navbar navbar-expand-lg  navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <a class="navbar-brand" href="#">Tuku.com</a>
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url()  ?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"></a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#"></a>
      </li>
            <li class="nav-item">
        <a class="nav-link disabled" href="<?= base_url('crud') ?>">Admin</a>
    </ul>
     <a href="<?= base_url('shop/cart') ?>"> 
      <span class="navbar-text fa fa-shopping-cart">
        Keranjang <?= $this->cart->total_items()  ?>
      </span>
      </a>
  </div>
</nav>

