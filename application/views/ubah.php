  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ubah Data Produk
        <small>Elektronik</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">crud</a></li>
        <li class="active">ubah</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Forms Data</h3>
        </div>
        <div class="box-body">
<!-- =========================================================================================================================== -->
           <?= form_open_multipart('crud/ubah/' . $prd['id']); ?>
               <!--  <input type="hidden" name="id" id="id">  -->
                <div class="form-group col-md-6">
                  <label for="nama_produk">Nama Produk</label>
                  <input type="text" class="form-control" id="nama_barang" name="nama_barang" value="<?= $prd["nama_barang"]  ?>">
                </div>

                <div class="form-group col-md-6">
                  <label for="harga">Harga</label>
                  <input type="text" class="form-control" id="harga" name="harga" value="<?= $prd["harga"]  ?>">
                </div>

                <div class="form-group col-md-6">
                  <label for="stok">Stok</label>
                  <input type="text" class="form-control" id="stok" name="stok" value="<?= $prd["stok"]  ?>">
                </div>

                <div class="form-group col-md-6">
                  <label for="gambar">Gambar</label>
                  <input type="text"  id="old"  name="old"  value="<?php echo $prd['gambar']   ?>">
                  <input type="file" id="gambar" name="userfile" >
                   
                </div>
                
                <div class="form-group">
                    <label for="deskripsi" class="col-form-label col-md-12">Deskripsi</label>
           
                    <div class="box-body pad">
              
                    <textarea class="textarea" name="deskripsi" id="deskripsi" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                            <?= $prd["deskripsi"] ?>
                          </textarea>
                    </div> 
                </div>
                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="">
                    <button  class="btn btn-danger">Kembali</button>
                    </a>
                  </div>
                </form>
<!-- ====================================================================================================================================== -->
        </div>
        <!-- /.box-body -->>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->