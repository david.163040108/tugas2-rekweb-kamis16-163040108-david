  <div class="content-wrapper">
    <div class="container">


    <div class="row mt-5">
          <table class="table table-sm ">
            <thead>
              <tr class="bg-success text-white">
                <th scope="col">No</th>
                <th scope="col">Hapus</th>
                <th scope="col">Nama Prodak</th>
                <th scope="col">Quantity</th>
                <th scope="col">Harga</th>
                <th scope="col">Subtotal</th>
              </tr>
            </thead>
            <tbody>
              <?php echo form_open('shop/updateCart'); ?>

              <?php 
                    $i = 1;
                    foreach ($this->cart->contents() as $items): 
              ?>
                  <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
              <tr>
                <th scope="row"><?= $i++ ?></th>
                <td><a href="<?= base_url('shop/hapusCart/'. $items['rowid']) ?>">hapus</a></td>
                <td><?=$items['name']  ?></td>
                <td>
                <?php echo form_input(array('name' => $i.'qty', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5')); ?>
                  
                </td>
                <td><?= number_format($items['price']) ?></td>
                <td align="right"><?= number_format($items['subtotal']) ?></td>
              </tr>
              <?php endforeach; ?>

            </tbody>
            <tfoot>
              <tr>
                <td align="center" colspan="5"> Total</td>
                <td align="right"><?= number_format( $this->cart->total())  ?></td>
              </tr>
            </tfoot>
          </table>
          <p class="mr-3"><?php echo form_submit('', 'Update Keranjang',"class='btn btn-outline-dark'"); ?></p>

          <p class="mr-3"><a href="<?= base_url('shop/clear_cart') ?>" class="btn btn-outline-info">Hapus Semua</a></p>


          <p class="mr-3"><a href="<?= base_url() ?>" class="btn btn-outline-warning">Check Out</a></p>
        </p>
    </div>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>